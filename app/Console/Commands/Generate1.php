<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Generate1 extends Command
{
    protected $signature = 'generate:db1';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('start:' . date('Y-m-d H:i:s'));

        $this->call('db:seed', [
            '--class' => 'MichalTablesSeeder'
        ]);

        $this->info('end:' . date('Y-m-d H:i:s'));
    }
}
