<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MSamochod;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(MSamochod::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));
    $v = $faker->vehicleArray();

    return [
        'marka' => $v['brand'],
        'model' => $v['model'],
        'moc' => $faker->numberBetween($min = 20, $max = 5000),
        'nadwozie' => $faker->vehicleType,
        'kolor' => $faker->colorName,
        'rok_produkcji' => $faker->year($max = 'now'),
        'skrzynia_biegow' => $faker->vehicleGearBoxType,
        'przebieg' => $faker->numberBetween($min = 1000, $max = 500000),
        'ilosc_cylindrow' => $faker->numberBetween($min = 4, $max = 16),
        'rodzaj_paliwa' => $faker->vehicleFuelType,
        'liczba_drzwi' => $faker->numberBetween($min = 2, $max = 6),
    ];
});
