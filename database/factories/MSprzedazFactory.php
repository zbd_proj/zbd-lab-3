<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\MSprzedaz;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(MSprzedaz::class, function (Faker $faker) {
    return [
        'cena' => $faker->numberBetween($min = 500, $max = 5000000),
        'id_klient' => 1,
        'id_samochod' => 1,
        'id_czas' => 1,
        'id_obszar' => 1,
    ];
});
