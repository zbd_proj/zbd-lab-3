<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSamochodTable extends Migration
{
    public function up()
    {
        Schema::create('m_samochod', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('marka');
            $table->string('model');
            $table->string('moc');
            $table->string('nadwozie');
            $table->string('kolor');
            $table->year('rok_produkcji');
            $table->string('skrzynia_biegow');
            $table->string('przebieg');
            $table->integer('ilosc_cylindrow');
            $table->string('rodzaj_paliwa');
            $table->integer('liczba_drzwi');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('m_samochod');
    }
}
