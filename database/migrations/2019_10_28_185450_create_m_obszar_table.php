<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMObszarTable extends Migration
{
    public function up()
    {
        Schema::create('m_obszar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('adres');
            $table->string('region');
            $table->string('kraj');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('m_obszar');
    }
}
