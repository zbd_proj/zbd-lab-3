<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMCzasTable extends Migration
{
    public function up()
    {
        Schema::create('m_czas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('miesiac');
            $table->integer('kwartal');
            $table->year('rok');
            $table->integer('dzien');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('m_czas');
    }
}
