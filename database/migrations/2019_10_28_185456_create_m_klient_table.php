<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMKlientTable extends Migration
{
    public function up()
    {
        Schema::create('m_klient', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('imie');
            $table->string('nazwisko')->nullable();
            $table->integer('wiek')->nullable();
            $table->string('plec')->nullable();
            $table->string('zarobki')->nullable();
            $table->string('telefon')->nullable();
            $table->text('adres');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('m_klient');
    }
}
