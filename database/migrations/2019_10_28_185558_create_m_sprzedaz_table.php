<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSprzedazTable extends Migration
{
    public function up()
    {
        Schema::create('m_sprzedaz', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cena');
            $table->unsignedBigInteger('id_klient');
            $table->unsignedBigInteger('id_samochod');
            $table->unsignedBigInteger('id_czas');
            $table->unsignedBigInteger('id_obszar');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('m_sprzedaz');
    }
}
