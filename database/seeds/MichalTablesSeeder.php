<?php

use Illuminate\Database\Seeder;

class MichalTablesSeeder extends Seeder
{
    public function run()
    {
        $ilosc = 10;

        factory(\App\MSamochod::class, $ilosc)->create();
        factory(\App\MObszar::class, $ilosc)->create();
        factory(\App\MCzas::class, $ilosc)->create();
        factory(\App\MKlient::class, $ilosc)->create();

        $sprzedazCount = \App\MSprzedaz::count();

        factory(\App\MSprzedaz::class, $ilosc)
            ->make()
            ->each(function ($s, $index) use ($sprzedazCount) {
                $s['id_klient'] = $sprzedazCount + $index + 1;
                $s['id_samochod'] = $sprzedazCount + $index + 1;
                $s['id_czas'] = $sprzedazCount + $index + 1;
                $s['id_obszar'] = $sprzedazCount + $index + 1;

                $s->save();
            });
    }
}
